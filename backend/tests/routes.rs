const TEST_ADDR: &str = "localhost:15832";

#[actix_rt::test]
async fn health_check_works() {
    spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .get(&format!("http://{}/api/health_check", TEST_ADDR))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}

#[actix_rt::test]
async fn api_nonexistant_fails() {
    spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .get(&format!("http://{}/api/not_a_thing", TEST_ADDR))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_client_error());
}

#[actix_rt::test]
async fn api_root_fails() {
    spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .get(&format!("http://{}/api", TEST_ADDR))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_client_error());
}

#[actix_rt::test]
async fn root_works() {
    spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .get(&format!("http://{}/", TEST_ADDR))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_success());
    assert!(response.content_length().unwrap() > 0);
}

#[actix_rt::test]
async fn api_lesson_fails() {
    spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .get(&format!("http://{}/api/lesson", TEST_ADDR))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_client_error());
}

#[actix_rt::test]
async fn serving_works() {
    spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .get(&format!("http://{}/index.html", TEST_ADDR))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_success());
    assert!(response.content_length().unwrap() > 0);
}

async fn spawn_app() {
    if let Ok(server) = backend::run(TEST_ADDR).await {
        tokio::spawn(server);
    }
}
