PRAGMA foreign_keys = ON;

CREATE TABLE lesson(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    teacher TEXT,
    is_elective BOOLEAN NOT NULL CHECK (is_elective IN (0,1)),
    weekday INTEGER NOT NULL CHECK (weekday < 7),
    hour_start INTEGER NOT NULL CHECK (hour_start >= 0 AND hour_start <= 24),
    hour_end INTEGER NOT NULL CHECK (hour_end >= 0 AND hour_end <= 24)
);

CREATE TABLE student_group(
    id INTEGER PRIMARY KEY,
    degree TEXT NOT NULL,
    specialty TEXT NOT NULL,
    year INTEGER NOT NULL,
    stream INTEGER NOT NULL,
    subgroup INTEGER NOT NULL,

    UNIQUE(degree, specialty, year, stream, subgroup)
);

CREATE TABLE lessons__student_groups(
    lesson_id INTEGER NOT NULL,
    student_group_id INTEGER NOT NULL,

    PRIMARY KEY(lesson_id, student_group_id),
    FOREIGN KEY(lesson_id) REFERENCES lesson(id),
    FOREIGN KEY(student_group_id) REFERENCES student_group(id)
);
