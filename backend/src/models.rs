use serde::{Deserialize, Serialize};
use sqlx::SqlitePool;

#[derive(Debug, Clone, PartialEq, Eq, sqlx::FromRow, Serialize, Deserialize)]
pub struct Lesson {
    id: u32,
    name: String,
    teacher: Option<String>,
    is_elective: bool,
    weekday: u8,
    hour_start: u8,
    hour_end: u8,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StudentGroup {
    #[serde(skip_deserializing)]
    pub id: u32,
    pub degree: String,
    pub specialty: String,
    pub year: u8,
    pub stream: u8,
    pub subgroup: u8,
}

impl Lesson {
    pub async fn get_all_elective(
        pool: &SqlitePool,
    ) -> sqlx::Result<Vec<Self>> {
        sqlx::query_as::<_, Lesson>(
            "
            SELECT *
            FROM lesson
            WHERE is_elective = 1
        ",
        )
        .fetch_all(pool)
        .await
    }

    pub async fn get_all_for_group(
        pool: &SqlitePool,
        sg: &StudentGroup,
    ) -> sqlx::Result<Vec<Self>> {
        sqlx::query_as::<_, Lesson>(
            "
            SELECT l.id, l.name, l.teacher, l.is_elective, l.weekday,
                l.hour_start, l.hour_end
            FROM lessons__student_groups AS lsg
                INNER JOIN lesson AS l
                ON lsg.lesson_id = l.id  AND l.is_elective = 0
                INNER JOIN student_group AS sg
                ON lsg.student_group_id = sg.id
                    AND sg.degree = ?
                    AND sg.specialty = ?
                    AND sg.year = ?
                    AND sg.stream = ?
                    AND sg.subgroup = ?
        ",
        )
        .bind(sg.degree.clone())
        .bind(sg.specialty.clone())
        .bind(sg.year)
        .bind(sg.stream)
        .bind(sg.subgroup)
        .fetch_all(pool)
        .await
    }
}
