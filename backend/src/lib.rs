use actix_web::{dev::Server, web, App, HttpServer};
use sqlx::SqlitePool;
use std::env;

mod handlers;
mod models;
mod routes;

pub const ADDRESS: &str = "localhost:3030";

pub async fn run(addr: &str) -> anyhow::Result<Server> {
    let _ = dotenv::dotenv();
    let pool = SqlitePool::connect(&env::var("DATABASE_URL")?).await?;
    let db = web::Data::new(pool);

    let server = HttpServer::new(move || {
        App::new().configure(routes::config).app_data(db.clone())
    })
    .bind(addr)?
    .run();

    Ok(server)
}
