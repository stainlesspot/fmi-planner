use crate::handlers;
use actix_files::{Files, NamedFile};
use actix_web::web;

async fn index_html() -> actix_web::Result<NamedFile> {
    Ok(NamedFile::open("../frontend/public/index.html")?)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/api")
            .route("/health_check", web::get().to(handlers::health_check))
            .route(
                "/lessons/elective",
                web::get().to(handlers::lessons::get_elective),
            )
            .route(
                "/lessons/required",
                web::get().to(handlers::lessons::get_required),
            ),
    )
    .service(
        Files::new("/", "../frontend/public")
            .index_file("index.html")
            .default_handler(web::get().to(index_html)),
    );
}
