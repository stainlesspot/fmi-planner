use backend::ADDRESS;

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    let server = backend::run(ADDRESS).await?;
    println!("Listening for requests at http://{}", ADDRESS);
    server.await?;

    Ok(())
}
