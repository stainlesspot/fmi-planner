use actix_web::{HttpResponse, Responder};

pub async fn health_check() -> impl Responder {
    HttpResponse::Ok()
}

pub mod lessons {
    use crate::models::{Lesson, StudentGroup};
    use actix_web::{web, HttpRequest, HttpResponse};
    use sqlx::SqlitePool;

    pub async fn get_elective(
        _request: HttpRequest,
        pool: web::Data<SqlitePool>,
    ) -> Result<HttpResponse, HttpResponse> {
        let lessons = Lesson::get_all_elective(&pool)
            .await
            .map_err(|_| HttpResponse::InternalServerError().finish())?;

        Ok(HttpResponse::Ok().json(lessons))
    }

    pub async fn get_required(
        _request: HttpRequest,
        sg: web::Query<StudentGroup>,
        pool: web::Data<SqlitePool>,
    ) -> Result<HttpResponse, HttpResponse> {
        let lessons = Lesson::get_all_for_group(&pool, &sg)
            .await
            .map_err(|_| HttpResponse::InternalServerError().finish())?;

        Ok(HttpResponse::Ok().json(lessons))
    }
}
