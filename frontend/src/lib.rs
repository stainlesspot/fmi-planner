#![recursion_limit = "1024"]

use wasm_bindgen::prelude::*;
use yew::prelude::*;

mod components;
mod models;
mod translate;

#[wasm_bindgen(start)]
pub fn run_app() {
    App::<components::Root>::new().mount_to_body();
}
