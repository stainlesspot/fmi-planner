use crate::{
    models::{
        extend_without_collision, fill_lesson_table, Cell, Lesson, FIRST_HOUR,
        LAST_HOUR,
    },
    translate,
};
use std::iter;
use yew::{
    format::{Json, Nothing},
    prelude::*,
    services::fetch::*,
};

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub required_lessons: Vec<Lesson>,
}

pub enum Msg {
    Noop,
    FetchElectives,
    GotElectives(Vec<Lesson>),
    ToggleRequired,
}

pub struct Schedule {
    link: ComponentLink<Self>,
    props: Props,
    task: Option<FetchTask>,
    elective_lessons: Option<Vec<Lesson>>,
    hide_required: bool,
}

impl Schedule {
    fn fetch_electives(&mut self) -> FetchTask {
        let callback = self.link.callback(
            move |response: Response<Json<anyhow::Result<Vec<Lesson>>>>| {
                let (meta, Json(data)) = response.into_parts();
                if meta.status.is_success() {
                    Msg::GotElectives(data.unwrap())
                } else {
                    Msg::Noop
                }
            },
        );
        let request =
            Request::get("/api/lessons/elective").body(Nothing).unwrap();

        FetchService::fetch(request, callback).unwrap()
    }
}

impl Component for Schedule {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            props,
            task: None,
            elective_lessons: None,
            hide_required: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Noop => {
                return false;
            }
            Msg::FetchElectives => {
                self.task = Some(self.fetch_electives());
            }
            Msg::GotElectives(lessons) => {
                self.elective_lessons = Some(lessons);
            }
            Msg::ToggleRequired => {
                self.hide_required = !self.hide_required;
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        if props != self.props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let mut lessons = if self.hide_required {
            vec![]
        } else {
            self.props.required_lessons.iter().collect::<Vec<_>>()
        };

        if let Some(ls) = &self.elective_lessons {
            lessons = extend_without_collision(&lessons, &ls)
        }

        let rows = fill_lesson_table(&lessons).into_iter().enumerate().map(
            |(weekday, row)| {
                let cells = row.into_iter().map(|cell| match cell {
                    Cell::Empty(_) => html! { <td class="empty" /> },
                    Cell::Full(lesson) => view_lesson(lesson),
                });
                html! {
                    <tr>
                        <td>{translate::weekday(weekday as u8)}</td>
                        {for cells}
                    </tr>
                }
            },
        );

        let header_row = iter::once(html! { <th /> })
            .chain((FIRST_HOUR..=LAST_HOUR).map(|i| html! { <th>{i}</th> }));

        let suggest = self.link.callback(|_| Msg::FetchElectives);
        let toggle_required = self.link.callback(|_| Msg::ToggleRequired);

        let toggle_button = html! {
            <button onclick=toggle_required>
            {format!("{} задължителните",
                     if self.hide_required { "покажи" } else { "скрии" })}
            </button>
        };
        let no_info = if self.props.required_lessons.is_empty() {
            html! {
                <p>{"няма данни за тази комбинация 😟"}</p>
            }
        } else {
            html! {}
        };

        html! {
            <>
                {toggle_button}
                <button onclick=suggest>{"предложи избираеми"}</button>
                {no_info}
                <table class="schedule">
                    <thead><tr> {for header_row} </tr></thead>
                    <tbody>{for rows}</tbody>
                </table>
            </>
        }
    }
}

fn view_lesson(lesson: Lesson) -> Html {
    let colspan = lesson.hour_end - lesson.hour_start;
    let mut classes = vec!["lesson"];
    if lesson.is_elective {
        classes.push("elective")
    }
    html! {
        <td class=classes colspan=colspan>
            <p>
                {
                    format!(
                        "{}{}",
                        lesson.name,
                        lesson.teacher.map(|t| format!(", {}", t))
                            .unwrap_or(String::new()),
                    )
                }
            </p>
        </td>
    }
}
