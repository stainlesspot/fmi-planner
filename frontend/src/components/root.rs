use crate::{components::Schedule, models::Lesson, translate::to_bg};
use std::collections::HashMap;
use yew::{
    format::{Json, Nothing},
    prelude::*,
    services::fetch::*,
};

macro_rules! svec {
    [ $($x:expr),* $(,)?] => {
        vec![ $($x.into()),* ]
    }
}

struct Availables {
    degrees: Vec<String>,
    specialties: Vec<String>,
    years: Vec<u8>,
    streams: Vec<u8>,
    subgroups: Vec<u8>,
}

pub struct Root {
    link: ComponentLink<Self>,
    selection: HashMap<String, String>,
    available: Availables,
    task: Option<FetchTask>,
    required_lessons: Option<Vec<Lesson>>,
}

pub enum Msg {
    Noop,
    Select(String, String),
    FetchLessons,
    GotLessons(Vec<Lesson>),
}

impl Root {
    fn new(link: ComponentLink<Self>) -> Self {
        Self {
            link,
            selection: HashMap::new(),
            available: Availables {
                degrees: svec!["bachelor", "master"],
                specialties: svec![
                    "computer-science",
                    "software-engineering",
                    "informatics",
                    "information-systems",
                ],
                years: vec![1, 2, 3, 4],
                streams: vec![1, 2],
                subgroups: vec![1, 2, 3, 4, 5, 6, 7, 8],
            },
            task: None,
            required_lessons: None,
        }
    }

    fn fetch_lessons(&mut self) -> FetchTask {
        let callback = self.link.callback(
            move |response: Response<Json<anyhow::Result<Vec<Lesson>>>>| {
                let (meta, Json(data)) = response.into_parts();
                if meta.status.is_success() {
                    Msg::GotLessons(data.unwrap())
                } else {
                    Msg::Noop
                }
            },
        );
        let request = Request::get(format!(
            "/api/lessons/required?degree={}&specialty={}&year={}&stream={}&subgroup={}",
            self.selection.get("degree").unwrap_or(&"".to_string()),
            self.selection.get("specialty").unwrap_or(&"".to_string()),
            self.selection.get("year").unwrap_or(&"".to_string()),
            self.selection.get("stream").unwrap_or(&"".to_string()),
            self.selection.get("subgroup").unwrap_or(&"".to_string()),
        ))
        .body(Nothing)
        .unwrap();

        FetchService::fetch(request, callback).unwrap()
    }

    fn view_select<T: ToString>(&self, name: &'static str, vals: &[T]) -> Html {
        let change_callback =
            self.link.callback(move |event: ChangeData| match event {
                ChangeData::Select(sel) => {
                    Msg::Select(name.to_string(), sel.value())
                }
                _ => Msg::Noop,
            });
        html! {
            <label>
                <p>{to_bg(name)}</p>
                <select name=name onchange=change_callback>
                    <option value="">{format!("<{}>", to_bg(name))}</option>
                    {for vals.iter().map(|v| {
                        html! {
                            <option value=v.to_string()>
                                {to_bg(&v.to_string())}
                            </option>
                        }
                    })}
                </select>
            </label>
        }
    }
}

impl Component for Root {
    type Message = Msg;
    type Properties = ();
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Root::new(link)
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Noop => {
                return false;
            }
            Msg::Select(name, value) => {
                self.selection.insert(name, value);
                self.task = Some(self.fetch_lessons());
            }
            Msg::FetchLessons => {
                self.task = Some(self.fetch_lessons());
            }
            Msg::GotLessons(lessons) => {
                self.required_lessons = Some(lessons);
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        let see = self.link.callback(|_| Msg::FetchLessons);
        html! {
            <div>
                <h1>{"фми-график"}</h1>
                <form name="student-group">
                    <p>{"избери:"}</p>
                    {self.view_select("degree", &self.available.degrees)}
                    {self.view_select("specialty", &self.available.specialties)}
                    {self.view_select("year", &self.available.years)}
                    {self.view_select("stream", &self.available.streams)}
                    {self.view_select("subgroup", &self.available.subgroups)}
                </form>
                <button onclick=see>{"виж"}</button>
                {
                    if let Some(lessons) = &self.required_lessons {
                        html! {
                            <Schedule required_lessons=lessons.clone() />
                        }
                    } else {
                        html!{}
                    }
                }
            </div>
        }
    }
}
