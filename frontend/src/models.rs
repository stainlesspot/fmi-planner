use serde::{Deserialize, Serialize};
use std::iter;

pub const FIRST_HOUR: u8 = 8;
pub const LAST_HOUR: u8 = 22;

pub const FIRST_WEEKDAY: u8 = 0;
pub const LAST_WEEKDAY: u8 = 6;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Lesson {
    pub id: u32,
    pub name: String,
    pub teacher: Option<String>,
    pub is_elective: bool,
    pub weekday: u8,
    pub hour_start: u8,
    pub hour_end: u8,
}

impl Lesson {
    pub fn collides_with(&self, other: &Lesson) -> bool {
        self.weekday == other.weekday
            && ((self.hour_start >= other.hour_start
                && self.hour_start < other.hour_end)
                || (self.hour_end > other.hour_start
                    && self.hour_end <= other.hour_end))
    }
}

pub enum Cell {
    Empty(u8),
    Full(Lesson),
}

pub fn fill_lesson_table(lessons: &[&Lesson]) -> Vec<Vec<Cell>> {
    (FIRST_WEEKDAY..=LAST_WEEKDAY)
        .map(|weekday| {
            let mut row = lessons
                .iter()
                .filter(|l| l.weekday == weekday)
                .collect::<Vec<_>>();
            row.sort_by_key(|l| l.hour_start);

            let mut prev_hour = FIRST_HOUR;
            let mut cells = row
                .into_iter()
                .flat_map(|l| {
                    let res = (prev_hour..l.hour_start)
                        .map(Cell::Empty)
                        .chain(iter::once(Cell::Full((*l).clone())));

                    prev_hour = l.hour_end;
                    res
                })
                .collect::<Vec<_>>();
            cells.extend((prev_hour..=LAST_HOUR).map(Cell::Empty));

            cells
        })
        .collect()
}

pub fn extend_without_collision<'a>(
    old_lessons: &[&'a Lesson],
    new_lessons: &'a [Lesson],
) -> Vec<&'a Lesson> {
    new_lessons.iter().fold(
        old_lessons.to_vec(),
        |mut acc: Vec<&'a Lesson>, lesson| {
            if acc.iter().all(|l| !l.collides_with(lesson)) {
                acc.push(lesson);
            }
            acc
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    fn lesson(weekday: u8, hour_start: u8, hour_end: u8) -> Lesson {
        Lesson {
            id: 0,
            name: String::new(),
            teacher: None,
            is_elective: false,
            weekday,
            hour_start,
            hour_end,
        }
    }

    #[test]
    fn test_lesson_colides_with_same_start() {
        assert!(lesson(0, 8, 9).collides_with(&lesson(0, 8, 10)));
    }
    #[test]
    fn test_lesson_colides_with_itself() {
        assert!(lesson(0, 10, 22).collides_with(&lesson(0, 10, 22)));
    }
    #[test]
    fn test_lesson_colides_with_same_end() {
        assert!(lesson(0, 10, 20).collides_with(&lesson(0, 0, 20)));
    }
    #[test]
    fn test_lesson_colides_with_same_end_rev() {
        assert!(lesson(0, 0, 20).collides_with(&lesson(0, 10, 20)));
    }
    #[test]
    fn test_lesson_colides_left_start_between() {
        assert!(lesson(0, 12, 20).collides_with(&lesson(0, 10, 15)));
    }
    #[test]
    fn test_lesson_colides_right_start_between() {
        assert!(lesson(0, 10, 14).collides_with(&lesson(0, 12, 15)));
    }
    #[test]
    fn test_lesson_colides_left_end_between() {
        assert!(lesson(0, 1, 12).collides_with(&lesson(0, 10, 15)));
    }
    #[test]
    fn test_lesson_colides_right_end_between() {
        assert!(lesson(0, 10, 12).collides_with(&lesson(0, 9, 11)));
    }
    #[test]
    fn test_lesson_does_not_collide() {
        assert!(!lesson(0, 1, 2).collides_with(&lesson(0, 9, 11)));
    }
    #[test]
    fn test_lesson_does_not_collide_adjacent() {
        assert!(!lesson(0, 1, 2).collides_with(&lesson(0, 2, 5)));
    }
    #[test]
    fn test_lesson_does_not_collide_adjacent_rev() {
        assert!(!lesson(0, 5, 7).collides_with(&lesson(0, 2, 5)));
    }
    #[test]
    fn test_lesson_does_not_collide_adjacent_diff_day() {
        assert!(!lesson(1, 5, 7).collides_with(&lesson(0, 2, 5)));
    }
    #[test]
    fn test_lesson_does_not_colide_with_itself_diff_day() {
        assert!(!lesson(0, 10, 22).collides_with(&lesson(5, 10, 22)));
    }
    #[test]
    fn test_lesson_does_not_colide_with_same_end_diff_day() {
        assert!(!lesson(4, 10, 20).collides_with(&lesson(0, 0, 20)));
    }

    #[test]
    fn test_fills_table_with_same_elements() {
        let lessons = vec![
            lesson(4, 10, 20),
            lesson(0, 4, 9),
            lesson(1, 11, 13),
            lesson(2, 11, 13),
            lesson(5, 12, 13),
            lesson(6, 12, 13),
            lesson(3, 12, 13),
        ];

        let actual = fill_lesson_table(&lessons.iter().collect::<Vec<_>>())
            .into_iter()
            .flat_map(|r| r.into_iter())
            .filter_map(|c| if let Cell::Full(l) = c { Some(l) } else { None })
            .collect::<Vec<_>>();

        assert_eq!(lessons.len(), actual.len());
        for l in &lessons {
            assert!(actual.contains(l));
        }
        for a in actual {
            assert!(lessons.contains(&a));
        }
    }

    #[test]
    fn test_extend_without_collision_removes_collisions() {
        let lessons = vec![
            lesson(4, 10, 20),
            lesson(0, 4, 9),
            lesson(1, 11, 13),
            lesson(2, 11, 13),
            lesson(5, 12, 13),
            lesson(6, 12, 13),
            lesson(3, 12, 13),
            lesson(0, 4, 9),
            lesson(1, 11, 13),
            lesson(2, 11, 13),
            lesson(5, 12, 13),
            lesson(6, 12, 13),
            lesson(3, 12, 13),
            lesson(0, 4, 9),
            lesson(1, 11, 13),
            lesson(2, 11, 13),
            lesson(5, 12, 13),
            lesson(6, 12, 13),
            lesson(3, 12, 13),
        ];

        let actual = extend_without_collision(&[], &lessons);

        for i in 0..actual.len() {
            for j in (i + 1)..actual.len() {
                assert!(!actual[i].collides_with(actual[j]));
            }
        }
    }
}
