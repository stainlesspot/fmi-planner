pub fn to_bg(s: &str) -> &str {
    match s {
        "degree" => "степен",
        "specialty" => "специалност",
        "year" => "курс",
        "stream" => "поток",
        "subgroup" => "група",

        "bachelor" => "бакалавър",
        "master" => "магистър",
        "computer-science" => "компютърни науки",
        "software-engineering" => "софтуерно инженерство",
        "informatics" => "информатика",
        "information-systems" => "информационни системи",
        _ => s,
    }
}

pub fn weekday(w: u8) -> &'static str {
    match w {
        0 => "понеделник",
        1 => "вторник",
        2 => "сряда",
        3 => "четвъртък",
        4 => "петък",
        5 => "събота",
        6 => "неделя",
        _ => "непознат ден от седмицата",
    }
}
