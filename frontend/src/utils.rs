macro_rules! svec {
    [ $($x:expr),* $(,)?] => {
        vec![ $($x.into()),* ]
    }
}
